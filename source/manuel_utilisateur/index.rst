.. _manuel_utilisateur:

#######################
Manuel de l'utilisateur
#######################

.. toctree::
   :numbered:
   :maxdepth: 3

   ergonomie/index.rst
   scrutin/index.rst
   export/index.rst
   traitement/index.rst
   parametrage/index.rst

