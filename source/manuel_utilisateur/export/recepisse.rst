.. _recepisse:

#################################################
Édition des récépissés aux assesseurs et délégués
#################################################

L'édition des récépissés aux assesseurs et délégués est disponible depuis le menu "Édition" option "Récépissé".

.. image:: ../../_static/trt_recepisse.png

Le lien "Assesseur(s) et Délégué(s)" déclenche la génération d'un mailing au format PDF comportant les lettres de récépissés.
