.. _export:

######
Export
######

Nous vous proposons dans ce chapitre de décrire les éditions openScrutin.

- Composition des bureaux

- Convocation des agents

- Convocation des présidents

- Récépissé

- Comment personnaliser les états en fonction de ses besoins


.. toctree::

    composition_bureau.rst
    convocation_agent.rst
    convocation_president.rst
    recepisse.rst
    personnalisation_etats.rst