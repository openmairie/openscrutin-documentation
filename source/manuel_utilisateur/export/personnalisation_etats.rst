.. _personnalisation_etats:

Tous les documents PDF, mailings et états, générés par openScrutin s'appuient sur un puissant générateur paramétrable.
Nous vous proposons dans ce chapitre de décrire la personnalisation des états fournis pour les personnaliser en fonction des besoins de votre collectivité.

##############################
Personnalisation des états PDF
##############################

Généralités sur les états
-------------------------

Les états sont composés d'un texte paramétrable ainsi que d'un ou plusieurs tableaux de données appelés "sous-états".

*N.B. : Les "États" sont aussi appelés "Lettres Types"*

Les états et les sous-états sont accessibles par le menu "Administration" option "État" ou "Sous-état".

Personnalisation des états
--------------------------

Le menu "Paramètrage" option "État" permet d'accéder à la liste des états.

.. image:: ../../_static/tab_etat.png

La liste des états pré-définis est :

- scrutin_president (utilise le sous-état scrutin_president) :

    Liste des présidents et suppléants.

    Cette édition est accessible via le **tableau d'affichage des scrutins, premier pictogramme d'édition**.

- candidature :

    Lettre de convocation des agents affectés au scrutin.

    Cette édition est accessible depuis **l'affichage d'un scrutin, onglet "Sélection Agent", en cliquant sur l'icône PDF à gauche de l'agent dans le tableau**.

    **ATTENTION : il est impératif de personnaliser cet état qui comporte des informations spécifiques**

- affectation_p

    Lettre d'affectation des présidents aux bureaux de vote.

    Cette édition est accessible depuis le **menu "Édition" option "Convocation des présidents"**.

    **ATTENTION : il est impératif de personnaliser cet état qui comporte des informations spécifiques**

- composition_bureau (utilise le sous-état composition_bureau)

    Édition de la composition du bureau de vote, comportant le président, le suppléant, le secrétaire, les assesseurs et les délégués.

    Cette édition est accessible depuis le **menu "Édition", option "Composition Bureau", puis lien sous "Édition composition des bureaux"**.

- affectation_r

    Lettre récépissé aux élus suite à leur affectation au bureau de vote.

    Cette édition est accessible depuis le **menu "Édition" option "Récépissé"**.

- scrutin_bureau (utilise le sous-état scrutin_bureau)

    Détail du bureau de vote.

    Cette édition est accessible via le **tableau d'affichage des scrutins, premier pictogramme d'édition**.

- candidature_p

    Lettre de convocation des agents.

    Cette édition est accessible depuis le **menu "Édition" option "Convocation des agents"**.

    **ATTENTION : il est impératif de personnaliser cet état qui comporte des informations spécifiques**

- affectation

    Courrier récépissé dans lequel l'élu est informé de son affectation pour le scrutin.

    Cette édition est accessible depuis **l'affichage d'un scrutin, onglet "Sélection Élu", en cliquant sur l'icône PDF à gauche de l'élu dans le tableau**.


Il suffit de cliquer sur le nom de l'état pour ouvrir son formulaire d'édition.

.. image:: ../../_static/form_etat.png

Les champs "Titre" et "Corps" contiennent les textes repris dans le document PDF généré.

**Attention** :

Ces documents contiennent des mots-clés de mise en page et des champs automatiques :

- les mots commençant par les caractères & et £ sont des champs de paramétrage. (exemple : £ville contient le nom de la ville)

- les mots entre crochets contiennent des informations tirées des données de travail. (exemple : [date_scrutin] contient le champ date de scrutin du formulaire de saisie du scrutin)

- les blocs de textes en gras sont précédés de "<b>" et suivis de "</b>". (exemple : la phrase avec <b>Aujourd'hui le £aujourdhui</b> en gras.)

.. note::

    Voir le guide du développeur openMairie (dans bibliographie)

