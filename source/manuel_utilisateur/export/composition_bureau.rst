.. _composition_bureau:

#########################################
Préparation de la composition des bureaux
#########################################

La préparation de la composition des bureaux est effectuée en préambule de l'édition de la composition des bureaux. **C'est une étape nécessaire avant toute autre édition.**

Cette fonction est accessible par le menu "Édition" option "Composition Bureau".

.. image:: ../../_static/trt_composition_bureau.png

**Avant toute édition, il est nécessaire de sélectionner un scrutin et "Confirmer le Traitement de Composition des bureaux".**

ce traitement met à jour la table temporaire composition_bureau utilisée lors de l'édition.


#####################################
Édition de la composition des bureaux
#####################################

Une fois le traitement effectué, on peut cliquer sur l'"Edition par bureau".

On accède alors à la liste des bureaux, pour lesquels l'édition de la composition du bureau est disponible au format PDF.

.. image:: ../../_static/trt_edition_composition_bureau.png

