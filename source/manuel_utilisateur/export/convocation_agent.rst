.. _convocation_agent:

###################################
Édition des convocations aux agents
###################################

L'édition des convocations aux agents est disponible depuis le menu "Édition" option "Convocation des Agents".

.. image:: ../../_static/trt_convocation_agent.png

Le lien "Envoi lettre Convocation" déclenche la génération d'un mailing au format PDF comportant les lettres de convocations.
