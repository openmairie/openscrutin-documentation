.. _convocation_president:

#######################################
Édition des convocations aux présidents
#######################################

L'édition des convocations aux présidents est disponible depuis le menu "Édition" option "Convocation des Présidents".

.. image:: ../../_static/trt_convocation_president.png

Le lien "Envoi lettre Convocation" déclenche la génération d'un mailing au format PDF comportant les lettres de convocations.
