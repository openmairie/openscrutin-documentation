.. _principes:


#############
Les principes
#############





Nous vous proposons maintenant d'utiliser openScrutin :

- de découvrir l'interface utilisateur

- de créer un scrutin et choisir un scrutin par défaut

- de gérer des candidatures d'agents à un poste dans un bureau

- d'affecter des élus dans un bureau
