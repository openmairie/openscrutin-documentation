.. _gestion_des_scrutins:

####################
Gestion des scrutins
####################

Nous vous proposons dans ce chapitre d'utiliser openScrutin après avoir décrit les
principes de l'application.

.. toctree::

    principes.rst
    interface_utilisateur.rst
    scrutin.rst
    agent.rst
    elu.rst


