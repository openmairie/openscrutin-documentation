.. _interface_utilisateur:

#######################
L'interface utilisateur
#######################

Écran d'accueil
---------------

Une fois connecté à oepnScrutin, vous accédez à l'écran d'accueil :

.. image:: ../../_static/principe_interface_accueil.png

Celui-ci comporte : 

1 - La barre d'actions utilisateurs permet de : 

    * changer votre mot de passe

    * vous déconnecter du logiciel

    * changer de base de données (pour accéder à une base de tests par exemple)

    * effectuer une recherche

    * revenir au tableau de bord d'accueil du logiciel

    Elle est toujours affichée, quel que soit l'opération en cours

2 - Le menu situé à gauche de l'écran

    Il permet d'accéder à l'ensemble des commandes du logiciel.

3 - L'entrée "scrutin par défaut" du tableau de bord (voir menu option scrutin)


Elle permet de définir le scrutin par défaut, pris en compte dans les autres actions au sein du logiciel.

.. image:: ../../_static/scrutin_defaut.png


Liste en tableau
----------------

Les données sont présentées sous la forme de tableaux (version 1.04 applicable sur la 2.0.0):

.. image:: ../../_static/principe_interface_tableau.png

Formulaire d'édition
--------------------

En cliquant sur une fiche on peut l'ouvrir en visualisation / modification  (version 1.04 applicable sur la 2.0.0):

.. image:: ../../_static/principe_interface_form.png