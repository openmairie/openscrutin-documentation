.. _elu:

#############
Saisir un élu
#############


Il est proposé de décrire dans ce paragraphe la saisie des élus et des affectations.


La liste des élus est accessible depuis le menu "Scrutin" option "Élu" :

.. image:: ../../_static/tab_elu.png


Le formulaire d'édition de l'élu s'affiche en cliquant sur la ligne de l'élu :

.. image:: ../../_static/form_elu.png


Les champs suivants sont affichés :

- code Élu : exemple:	EL02

- Nom

- Prénom

- Nom JF

- Date de Naissance

- Lieu de Naissance

- Adresse

- Code Postal

- Ville


####################
Affectation des élus
####################

L'affectation des élus se fait dans l'onglet "affectation" :

.. image:: ../../_static/tab_affectation.png

Le formulaire suivant est affiché :

.. image:: ../../_static/form_affectation.png


Les champs suivants sont affichés :

- élection par défaut (non modifiable)

- période : suivant la table période

- poste : suivant la table poste (pour élu)

- Candidat : suivant la saisie en scrutin et uniquement pour les délégués

- Note : texte libre

- Décision : Oui (case cochée) : sera pris en compte dans les traitements et éditions


.. note::

    Il est possible de basculer les affectations d'un scrutin précédent (voir chapitre traitement)

