.. _scrutin:

############################
Saisir et choisir un scrutin
############################

Il est proposé de décrire dans ce paragraphe la saisie et la consultation des scrutins.

Liste des scrutins
------------------

L'accès à la liste des scrutins se fait par le menu "Scrutin", option "Scrutin". Les scrutins sont présentés dans un tableau d'affichage :

.. image:: ../../_static/tab_scrutin.png

Il est possible de créer un scrutin en cliquant sur le bouton "+" ou de modifier un scrutin existant en cliquant sur le texte du scrutin.

Le formulaire de saisie apparaît alors.

Formulaire de saisie
--------------------

L'accès se fait en consultation :

.. image:: ../../_static/consulter_scrutin.png

En appuyant sur "modification" : 

.. image:: ../../_static/form_scrutin.png

* L'onglet "Candidat" permet de saisir les candidats à l'élection (pour les délégués de listes)

* Les onglets "Sélection Agent" et "Sélection Élu" permettent de visualiser les agent et les élus retenus pour le scrutin.


Le formulaire de saisie comporte les informations suivantes :

- Code Scrutin 	exemple MUN08-1

- Libellé

- Canton (suivant la table canton)

- Tour (1 ou 2)

- Date du Scrutin

- Soldé : le scrutin ne peut plus être modifié

- Convocation Agent 	: texte inséré dans la convocation agent (date de la réunion préparatoire)

- Convocation Président : texte inséré dans la convocation agent (date de la réunion préparatoire)

Il est possible d'avoir plusieurs scrutins non soldés en même temps pour gérer plusieurs élections.


Candidats
---------

La saisie des candidats se fait depuis l'onglet "Candidat".

Les candidats de l'election seront associés à une affectation d'élus (délégué titulaire ou suppléant)

.. image:: ../../_static/tab_candidat.png

Édition des courriers aux agents et élus
----------------------------------------

Il est possible de faire une édition d'une acceptation de candidature d'un agent et une édition informant
l'affectation à un élu.

Ces éditions se font depuis l'onglet correspondant, depuis le formulaire de saisie du scrutin.
(version 1.04 applicable sur la 2.0.0):

.. image:: ../../_static/edition_affectation.png

Scrutin par défaut
------------------

Le choix du scrutin à gérer pour les traitements se fait dans "scrutin par défaut".

Cette fonction est disponible depuis le menu "Scrutin", option "Scrutin par défaut".

.. image:: ../../_static/scrutin_defaut.png
