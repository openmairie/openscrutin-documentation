.. _agent:


###############
Saisir un agent
###############


Il est proposé de décrire dans ce paragraphe la saisie d'agents.

Les agents sont accessibles dans le menu "Scrutin" option "Agent".

.. image:: ../../_static/tab_agent.png


Il est possible de créer ou modifier ou supprimer un  agent ans le formulaire ci dessous

.. image:: ../../_static/form_agent.png


Les champs à saisir sont les suivants :


- Agent

- Nom

- Prénom

- Adresse

- Code Postal

- Ville

- Téléphone

- Service : suivant la table service

- Poste : suivant la table poste

- Grade : suivant la table grade

#################################
Saisir une candidature d'un agent
#################################

Les candidatures sont accessibles dans l'onglet "candidature" du formulaire d'édition de l'agent.

.. image:: ../../_static/tab_candidature.png


Il est possible de créer ou modifier ou supprimer une  candidature dans le formulaire ci dessous :

.. image:: ../../_static/form_candidature.png


Les champs à saisir sont les suivants :

- scrutin

- période suivant la table période (matin, après-midi ou journée)

- poste suivant la table poste

- bureau affecté suivant la table bureau

- récupération : choix de l'agent : si récupération est coché, il ne sera pas payé en heure supplémentaire (traitement affectation heure)

- note : texte libre

- décision : oui : la candidature est acceptée et sera prise dans les traitements et éditions

- heures début et fin : heures retenues pour le paiement des heures supplémentaires ou récupération


.. note::

    Il est possible de basculer les candidatures d'un scrutin précédent (voir chapitre traitement).

