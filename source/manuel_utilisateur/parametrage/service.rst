.. _service:


#################
Saisir un service
#################


Il est proposé de décrire dans ce paragraphe de décrire la saisie des services
dans le menu paramétrage.

.. image:: ../../_static/tab_service.png


Il est possible de créer ou modifier un service dans le formulaire ci dessous :

.. image:: ../../_static/form_service.png


Il est saisie :

- le code service (uniquement lors de la création)

- le libelle


