.. _grade:

###############
Saisir un grade
###############


Il est proposé de décrire dans ce paragraphe de décrire la saisie des grades
dans le menu paramétrage.

.. image:: ../../_static/tab_grade.png


Il est possible de créer ou modifier un grade dans le formulaire ci dessous :

.. image:: ../../_static/form_grade.png


Il est saisie :

- le code grade (uniquement lors de la création)

- le libelle

