.. _poste:


###############
Saisir un poste
###############


Il est proposé de décrire dans ce paragraphe la saisie des postes
dans le menu paramétrage.

.. image:: ../../_static/tab_poste.png


Il est possible de créer ou modifier un poste dans le formulaire ci-dessous :

.. image:: ../../_static/form_poste.png


Les données saisies sont :

- Code poste (exemple : secretaire)

- Affectation ou candidature

- Ordre pour affectation : l'ordre est pris en compte dans les éditions

Il n'est pas possible de supprimer :

- secretaire et planton

- president, president adjoint, assesseur titulaire et suppleant, delegue titulaire et suppleant

Ces postes sont utilisés dans les traitements.