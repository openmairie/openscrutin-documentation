.. _principes_parametrage:

############################
Les principes du paramétrage
############################


Il y a de plusieurs tables à paramétrer :

* Liées à votre organisation électorale :

    - canton pour élection partielle

    - bureau de vote

    - poste (élus ou agents)

    - période


* Liées à votre organisation municipale :

    - les services des agents

    - les grades des agents
    
Vous pouvez paramétrer votre application :

- dans dyn/var.inc

ou votre collectivité dans

- administration -> om_parametre