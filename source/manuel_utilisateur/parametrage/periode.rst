.. _periode:


##################
Saisir une période
##################


Il est proposé de décrire dans ce paragraphe la saisie des périodes
dans le menu paramétrage.

.. image:: ../../_static/tab_periode.png


Il est possible de créer ou modifier une période dans le formulaire ci-dessous :

.. image:: ../../_static/form_periode.png


Les données saisies sont :

- Code période

- Libellé

- Heure de début

- Heure de fin

les heures de début et fin seront affectées aux candidatures d'agent (voir traitement affectation_heure)

Il n'est pas possible de supprimer :

- matin

- apres-midi

- journee

Ces périodes sont utilisées dans les traitements.



