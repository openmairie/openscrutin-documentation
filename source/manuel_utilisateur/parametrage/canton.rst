.. _canton:

################
Saisir un canton
################



Il est proposé de décrire dans ce paragraphe la saisie des cantons
dans le menu paramétrage.

.. image:: ../../_static/tab_canton.png


Il est possible de créer ou modifier un canton dans le formulaire ci-dessous :

.. image:: ../../_static/form_canton.png


Les données saisies sont :

- Code canton

- Libellé


Le code canton sert à ne prendre qu'une partie des bureaux.

Il n'est pas possible de supprimer le canton T : tous.


