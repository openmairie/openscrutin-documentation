.. _parametrage:

###########
Paramétrage
###########


Nous vous proposons dans ce chapitre de paramétrer openScrutin pour l'adapter à votre organisation :

- les principes du paramétrage

- les cantons

- les bureaux de vote

- les périodes

- la postes pour les candidatures ou affectations

- les services des agents

- les grades des agents


.. toctree::

    principes_parametrage.rst
    canton.rst
    bureau.rst
    periode.rst
    poste.rst
    service.rst
    grade.rst
    param.rst
