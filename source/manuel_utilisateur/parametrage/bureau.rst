.. _bureau:

################
Saisir un bureau
################

Il est proposé de décrire dans ce paragraphe la saisie des
bureaux dans le menu paramétrage.


Les bureaux sont listés dans le formulaire suivant :

.. image:: ../../_static/tab_bureau.png


Il est possible de créer ou modifier un bureau dans le formulaire ci-dessous :

.. image:: ../../_static/form_bureau.png


Les données saisies sont :

- Code bureau (exemple:	02)

- Libellé du bureau

- Canton suivant la table canton

- Adresse 1

- Adresse 2

- Code Postal

- Ville

Il n'est pas possible de supprimer le bureau T (Tous) qui est utilisé dans les traitements.