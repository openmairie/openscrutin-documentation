.. _transfert_affectation:


########################
Transfert d'affectations
########################

Nous vous proposons dans ce chapitre de décrire le transfert d'affectations.

Le transfert d'affectations est accessible depuis le menu "Traitement" option "Transfert Affectation".

.. image:: ../../_static/trt_transfert_affectation.png

Le formulaire de traitement comporte les champs suivants :

- Sélectionnez un Scrutin Soldé : scrutin depuis lequel les affectations seront prises

- Sélectionnez un Scrutin : scrutin sur lequel les affectations seront appliquées

- Sélectionnez un Poste : poste traité

- Sélectionnez un Candidat : Candidat traité ou Tous

Le traitement est déclenché par le bouton "Confirmez le Transfert".
