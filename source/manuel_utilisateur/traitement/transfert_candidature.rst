.. _transfert_candidature:


##########################
Transfert des candidatures
##########################

Nous vous proposons dans ce chapitre de décrire le transfert de candidatures.

Le transfert de candidatures est accessible depuis le menu "Traitement" option "Transfert Candidature". Ce traitement permet de récupérer les candidatures enregistrées dans un scrutin antérieur, et les ré-appliquer au scrutin courant.

.. image:: ../../_static/trt_transfert_candidature.png

Le formulaire de traitement comporte les champs suivants :

- Sélectionnez un Scrutin Soldé : scrutin depuis lequel les affectations seront prises

- Sélectionnez un Scrutin : scrutin sur lequel les affectations seront appliquées

- Sélectionnez un Poste : poste traité

Le traitement est déclenché par le bouton "Confirmez le Transfert".
