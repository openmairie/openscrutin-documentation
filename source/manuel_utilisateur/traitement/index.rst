.. _traitement:

##########
Traitement
##########


Nous vous proposons dans ce chapitre de décrire les traitements openScrutin.

- L'affectation des heures

- Le transfert de candidatures

- Le transfert d'affectations

.. toctree::

    affectation_heure.rst
    transfert_candidature.rst
    transfert_affectation.rst
