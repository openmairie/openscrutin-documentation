.. _affectation_heure:


##################################
Affectation automatique des heures
##################################

Nous vous proposons dans ce chapitre de décrire l'affectation automatique des heures.

L'affectation des heures est accessible depuis le menu "Traitement" option "Affectation des Heures".

.. image:: ../../_static/trt_affectation_heure.png

Il est nécessaire de sélectionner le scrutin puis cliquer sur le bouton "Confirmation de l'affectation des heures".

