.. openscrutin documentation master file.

=============================
openScrutin 2.0 documentation
=============================

.. note::

    Cette création est mise à disposition selon le Contrat Paternité-Partage des
    Conditions Initiales à l'Identique 2.0 France disponible en ligne
    http://creativecommons.org/licenses/by-sa/2.0/fr/ ou par courrier postal à
    Creative Commons, 171 Second Street, Suite 300, San Francisco,
    California 94105, USA.


openScrutin est un outil de gestion de la composition des bureaux de vote pour
les élections politiques :

* plantons et secrétaires (candidature des agents)
* présidents, vice présidents, assesseurs, délégués (affectation des élus)

Gestion des candidatures des fonctionnaires, affectation des élus, publipostage
des convocations, tableau récapitulatif des heures, état de la composition des
bureaux sont autant d'outils pratiques mis a disposition des services élections
à un moment plutôt critique dans la gestion du service.

Opérationnel depuis de nombreuses années, openScrutin fait partie du triptyque
openElec - openResultat - openScrutin. Ces trois modules composent aujourd'hui
l'alternative libre avec une architecture full web, aux solutions propriétaires.

Ce document a pour but de guider les utilisateurs et les développeurs dans la
prise en main du projet.

Il vous est présenté dans une première partie le manuel utilisateur et dans une
deuxiéme partie le guide du développeur.

Bonne lecture et n'hésitez pas à nous faire part de vos remarques à l'adresse
suivante : contact@openmairie.org !


Manuel de l'utilisateur
=======================

.. toctree::

   manuel_utilisateur/index.rst


Guide du développeur
====================

.. toctree::

   guide_developpeur/index.rst


Bibliographie
=============

* http://www.openmairie.org/telechargement/openMairie-Guidedudveloppeur.pdf/view


Contributeurs
=============

(par ordre alphabétique)

* `atReal <http://www.atreal.fr>`_
* Thierry Benita
* Florent Michon
* François Raynaud
