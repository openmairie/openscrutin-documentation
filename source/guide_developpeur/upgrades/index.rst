.. _upgrades:

##############
Mises à niveau
##############

***********************************************
Mettre à niveau depuis openScrutin 1.x vers 2.0
***********************************************

La version 2.0.0 n'est disponnible que sous postgresql et la version 1.04 fonctionne avec mysql.

Le transfert ne peut se faire que suite à une analyse de votre base et à la modification du jeu de données pour
correspondre au nouvelles exigences de la base postgresql, du nouveau modèle de données openscrutin et de celui du

Le passage de mysql (moteur myIsam) à postgresql, exige une plus forte cohérence des données 

- dans l'intégrité référentielle des clés secondaires, les clés numériques à 0 ou vide ne sont pas acceptées (mettre à null)

- les format de date 0000/00/00 ne sont pas acceptés : il faut les mettre à null (elu, scrutin)

- les formats d'heure 00:00 ne sont pas acceptés et il faut mettre 00:00:00 (période, agent)


Attention aux types de champ qui changent dans init_metier.sql :

- dans la table scrutin : solde est un champ boolean : remplacer 'Oui' par true et '' par false

- dans elu : la clé primaire elu est numérique

- dans affectation : le candidat est lié à la clé numérique candidat et ce n est plus un libellé ::

    -- mettre le nom du candidat dans une zone  temp dans affectation
    
    -- requete de mise à jour du champ candidat d affectation
    
    update openscrutin.affectation b set candidat = a.candidat from openscrutin.candidat a
    where a.nom = b.temp;


- dans agent : le champ poste s appelle telephone_pro. 

- dans la table candidature : decision et recuperation sont des champs booleans : remplacer 'Oui' par true et '' par false



Nouveaux champs dans la comptabilité du framework om_4.3.3

- dans om_utilisateur : il est rajouter un champ om_collectivite obligatoire. La clé secondaire profil s appelle om_profil et la
  clé primaire s appelle om_utilisateur.

